#!/bin/bash

echo -e "1) Install build depends for Hyprland...\n"
echo -e "2) Download and prepare Hyprland source...\n"
echo -e "3) Download and prepare Wayland Protocols...\n"
echo -e "4) Download and prepare Wayland...\n"
echo -e "5) Download and prepare libdisplay-info...\n"
echo -e "6) Download and prepare libliftoff...\n"
echo -e "7) Build and install Hyprland!\n"

echo -e "Enter option: "
read OPTION

case $OPTION in
	1)
		sudo apt-get install -y meson wget build-essential ninja-build cmake-extras cmake gettext gettext-base fontconfig libfontconfig-dev libffi-dev libxml2-dev libdrm-dev libxkbcommon-x11-dev libxkbregistry-dev libxkbcommon-dev libpixman-1-dev libudev-dev libseat-dev seatd libxcb-dri3-dev libvulkan-dev libvulkan-volk-dev  vulkan-validationlayers-dev libvkfft-dev libgulkan-dev libegl-dev libgles2 libegl1-mesa-dev glslang-tools libinput-bin libinput-dev libxcb-composite0-dev libavutil-dev libavcodec-dev libavformat-dev libxcb-ewmh2 libxcb-ewmh-dev libxcb-present-dev libxcb-icccm4-dev libxcb-render-util0-dev libxcb-res0-dev libxcb-xinput-dev xdg-desktop-portal-wlr hwdata libpango1.0-dev check libgtk-3-dev libsystemd-dev xwayland libgbm-dev
		;;

	2)
		wget https://github.com/hyprwm/Hyprland/releases/download/v0.29.1/source-v0.29.1.tar.gz && tar -xvf source-v0.29.1.tar.gz
		
		;;

	3)
		wget https://gitlab.freedesktop.org/wayland/wayland-protocols/-/releases/1.32/downloads/wayland-protocols-1.32.tar.xz && tar -xvJf wayland-protocols-1.32.tar.xz && cd wayland-protocols-1.32 && mkdir build && cd build && meson setup --prefix=/usr --buildtype=release && ninja && sudo ninja install && cd ../..

		;;

	4)
		wget https://gitlab.freedesktop.org/wayland/wayland/-/releases/1.22.0/downloads/wayland-1.22.0.tar.xz && tar -xvJf wayland-1.22.0.tar.xz && cd wayland-1.22.0 && mkdir build && cd build && meson setup .. --prefix=/usr --buildtype=release -Ddocumentation=false && ninja && sudo ninja install && cd ../..
		;;

	5)
		wget https://gitlab.freedesktop.org/emersion/libdisplay-info/-/releases/0.1.1/downloads/libdisplay-info-0.1.1.tar.xz && tar -xvJf libdisplay-info-0.1.1.tar.xz && cd libdisplay-info-0.1.1 &&mkdir build && cd build && meson setup --prefix=/usr --buildtype=release && ninja && sudo ninja install && cd ../..

		;;

	6)
		git clone https://gitlab.freedesktop.org/emersion/libliftoff.git && cd libliftoff && meson build/ && ninja -C build/ && cd build && sudo ninja install && cd ../..

		;;

	7)
		echo -e "WARNING:  BE SURE TO EDIT THE MAKEFILE TO CHANGE THE PREFIX TO /usr!!!"
		chmod a+rw hyprland-source &&  cd hyprland-source && sudo make install

		;;
esac


